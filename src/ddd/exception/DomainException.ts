export class DomainException extends Error {
    private errorCode: string
  
    constructor(errorCode: string, message: string) {
      super(message)
      this.name = 'DomainException'
      this.errorCode = errorCode
      this.message = message
    }
  }
  