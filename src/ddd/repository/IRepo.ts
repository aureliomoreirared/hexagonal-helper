import { AggregateRoot } from "../aggregate/AggregateRoot";

export interface IRepo<T extends AggregateRoot> {
  insert(entity: T): Promise<T>
  update(entity: T): Promise<void>
  delete(entity: T): Promise<void>
  findById(id: T['id']): Promise<T | null>
}
