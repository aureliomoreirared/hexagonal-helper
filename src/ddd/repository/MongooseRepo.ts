import { Model } from 'mongoose'
import { AggregateRoot } from '../aggregate/AggregateRoot'
import { IEventBus } from '../bus/IEventBus'
import { IRepo } from './IRepo'

export abstract class MongooseRepo<T extends AggregateRoot> implements IRepo<T> {
  protected bus: IEventBus
  protected model: Model<T>

  constructor(model: Model<T>, bus: IEventBus) {
    this.bus = bus
    this.model = model
  }

  async insert(entity: T): Promise<T> {
    const result = (await this.model.create(entity)) as any
    entity.id = result._id.toString()
    await this.bus.publish(entity)
    return entity
  }

  async update(entity: T): Promise<void> {
    await this.model.findByIdAndUpdate(entity.id, entity as any)
    await this.bus.publish(entity)
  }

  async delete(entity: T): Promise<void> {
    await this.model.deleteOne({ id: entity.id })
    await this.bus.publish(entity)
  }

  async findById(id: T['id']): Promise<T | null> {
    const doc = (await this.model.findById(id)) as any
    return doc ? doc.toObject() : null
  }
}
