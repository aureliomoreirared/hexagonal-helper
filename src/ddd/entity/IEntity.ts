export interface IEntity<TId extends {} = any> {
    id: TId
}
  