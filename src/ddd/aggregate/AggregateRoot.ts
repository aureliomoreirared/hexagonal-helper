import { DomainEvent } from "../domain-event/DomainEvent"
import { IEntity } from "../entity/IEntity"

export abstract class AggregateRoot<TId extends {} = any> implements IEntity<TId> {
  abstract id: TId

  private readonly events: DomainEvent[] = []

  protected addEvent(event: DomainEvent): void {
    this.events.push(event)
  }

  getEvents(): DomainEvent[] {
    return this.events.splice(0, this.events.length)
  }

  toJSON(): Omit<this, 'events'> {
    const { events: _, ...clone } = this

    return clone
  }
}
