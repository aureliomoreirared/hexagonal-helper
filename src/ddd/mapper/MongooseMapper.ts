import { AggregateRoot } from '../aggregate/AggregateRoot'
import { ClassConstructor, plainToClass } from 'class-transformer'
import { Document } from 'mongoose'


export abstract class Mapper<T extends AggregateRoot> {
  private type: ClassConstructor<T>
  private targets: any[]

  constructor(type: ClassConstructor<T>, targets: any[] = []) {
    this.type = type
    this.targets = targets
  }

  map(doc: Document): T {
    const result = plainToClass(this.type, doc.toJSON(), {
      excludePrefixes: ['_'],
      targetMaps: this.targets,
    })

    result.id = doc._id

    return result
  }
}
